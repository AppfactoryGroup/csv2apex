#!/bin/bash

set -e

if [ "$1" = 'heroku' ]
then
	url="https://csv2apex.herokuapp.com"
else
	url="http://localhost:${PORT:-8080}"
fi
if [ "$(curl -XPOST -F prefix=myapi_ -F csv=@data/static/example.csv "$url" --output /tmp/csv2apex.zip -w '%{http_code}')" != 200 ]; then
	printf '\nERROR:\n'
	cat /tmp/csv2apex.zip
	printf '\n\n'
	rm /tmp/csv2apex.zip
	exit 1
fi

# unzip -c /tmp/csv2apex.zip
if ! [ -d /tmp/testcsv2apex ]
then
	printf "Create new sfdx project\n"
	cd /tmp/
	sf project generate --name=testcsv2apex
	cd testcsv2apex
	printf "Creating scratch org\n"
	sf org create scratch --alias=testcsv2apex --set-default --definition-file=config/project-scratch-def.json
	mkdir -p force-app/main/default/classes/
else
	printf "Use existing sfdx project\n"
	cd /tmp/testcsv2apex
fi
unzip -o -d /tmp/testcsv2apex/force-app/main/default/classes/ /tmp/csv2apex.zip
sf project deploy start
sf apex run test --wait 20 --code-coverage --result-format human
