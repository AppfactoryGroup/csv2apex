FROM rust:1.76-buster as builder

RUN USER=root cargo new csv2apex
WORKDIR /usr/local/csv2apex
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
COPY src/ ./src
RUN cargo build --release

# 2: Copy the exe and extra files ("static") to an empty Docker image
FROM rust:1.76-slim-buster
COPY --from=builder /usr/local/csv2apex/target/release/csv2apex .
COPY data/ ./data
USER 1000
CMD ["./csv2apex"]
