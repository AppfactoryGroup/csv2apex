/*
CSV2Apex - generate strongly typed apex classes for parsing from and serializing to JSON
Copyright (C) 2021  App Factory Group <support@appfactorygroup.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::cell::RefCell;
use std::collections::HashMap;
use std::io::Read;
use std::io::Seek;
use std::rc::Rc;

use bytes::Buf;
use futures::TryStreamExt;
use serde::Deserialize;
use warp::{http, Filter};

use log::{debug, info};

mod code_gen;
mod util;

// https://gendignoux.com/blog/2021/04/01/rust-async-streams-futures-part1.html
// https://blog.logrocket.com/file-upload-and-download-in-rust/ & https://github.com/zupzup/warp-upload-download-example/blob/main/src/main.rs

type Resp = http::Response<Vec<u8>>;

#[derive(Deserialize, Debug, Clone)]
pub struct Record {
    object: String,
    field: String,
    data_type: String,
    list: Option<String>,
    description: Option<String>,
    apex_name: Option<String>,
    endpoint: Option<String>,
}

pub struct DefaultType {
    name: &'static str,
    parse: &'static str,
    write: &'static str,
    test_value: &'static str,
}

fn check_char(c: char) -> bool {
    !c.is_ascii_alphanumeric() && c != '_'
}

fn process_csv(prefix: String, csv: String) -> Result<Resp, Resp> {
    debug!("process_csv: prefix {}", prefix);
    let mut rdr = csv::ReaderBuilder::new().from_reader(csv.as_bytes());
    let mut objs_by_name: HashMap<String, Rc<RefCell<Vec<Record>>>> = HashMap::new();
    for result in rdr.deserialize() {
        let mut record: Record = match result {
            Ok(r) => r,
            Err(e) => {
                return Err(util::reply(
                    http::StatusCode::BAD_REQUEST,
                    format!("invalid csv: {}", e).into_bytes(),
                ));
            }
        };

        if record.list.is_none() || record.list.as_ref().unwrap().is_empty() {
            let lowercase_data_type = record.data_type.to_lowercase();
            if lowercase_data_type.starts_with("list<") && lowercase_data_type.ends_with(">") {
                record.list = Some(String::from("true"));
                record.data_type = String::from(&record.data_type[5..record.data_type.len() - 1]);
            } else if lowercase_data_type.ends_with("[]") {
                record.list = Some(String::from("true"));
                record.data_type = String::from(&record.data_type[0..record.data_type.len() - 2]);
            } else {
                record.list = Some(String::from("false"));
            }
        } else {
            record.list = Some(record.list.as_ref().unwrap().to_lowercase());
        }

        if record.apex_name.is_none() || record.apex_name.as_ref().unwrap().is_empty() {
            record.apex_name = Some(String::from("p_") + &record.field);
        }
        record.apex_name = Some(
            record
                .apex_name
                .as_ref()
                .unwrap()
                .replace(&check_char, "_")
                .replace("__", "_")
                .replace("__", "_")
                .trim_end_matches("_")
                .to_string(),
        );

        debug!("{:?}", record);
        let obj = match objs_by_name.get(&record.object) {
            Some(o) => o.clone(),
            None => {
                let obj = Rc::new(RefCell::new(Vec::new()));
                objs_by_name.insert(record.object.clone(), obj.clone());
                obj
            }
        };
        {
            let mut obj = obj.borrow_mut();
            obj.push(record.clone());
        }
    }
    let mut buf: Vec<u8> = Vec::new();
    let mut cur = std::io::Cursor::new(Vec::new());
    {
        let mut zip = zip::ZipWriter::new(&mut cur);

        // write base object
        code_gen::write_base_object(&mut zip, &prefix)?;

        // write exception class
        code_gen::write_exception(&mut zip, &prefix)?;

        // write utilities class

        let mut default_types: HashMap<String, DefaultType> = HashMap::new();
        default_types.insert(
            String::from("Boolean"),
            DefaultType {
                name: "Boolean",
                parse: "parser.getBooleanValue()",
                write: "gen.writeBoolean(value)",
                test_value: "true",
            },
        );
        default_types.insert(
            String::from("Date"),
            DefaultType {
                name: "Date",
                parse: "parser.getDateValue()",
                write: "gen.writeDate(value)",
                test_value: "Date.today()",
            },
        );
        default_types.insert(
            String::from("Datetime"),
            DefaultType {
                name: "Datetime",
                parse: "parser.getDatetimeValue()",
                write: "gen.writeDatetime(value)",
                test_value: "Datetime.now()",
            },
        );
        default_types.insert(
            String::from("Decimal"),
            DefaultType {
                name: "Decimal",
                parse: "parser.getDecimalValue()",
                write: "gen.writeNumber(value)",
                test_value: "54.321",
            },
        );
        default_types.insert(
            String::from("Double"),
            DefaultType {
                name: "Double",
                parse: "parser.getDoubleValue()",
                write: "gen.writeNumber(value)",
                test_value: "123.45",
            },
        );
        default_types.insert(
            String::from("Id"),
            DefaultType {
                name: "Id",
                parse: "parser.getIdValue()",
                write: "gen.writeId(value)",
                test_value: "'001S000001Np8YKIAZ'",
            },
        );
        default_types.insert(
            String::from("Integer"),
            DefaultType {
                name: "Integer",
                parse: "parser.getIntegerValue()",
                write: "gen.writeNumber(value)",
                test_value: "123",
            },
        );
        default_types.insert(
            String::from("Long"),
            DefaultType {
                name: "Long",
                parse: "parser.getLongValue()",
                write: "gen.writeNumber(value)",
                test_value: "9876543210l",
            },
        );
        default_types.insert(
            String::from("String"),
            DefaultType {
                name: "String",
                parse: "parser.getText()",
                write: "gen.writeString(value)",
                test_value: "'test'",
            },
        );
        default_types.insert(
            String::from("Time"),
            DefaultType {
                name: "Time",
                parse: "parser.getTimeValue()",
                write: "gen.writeTime(value)",
                test_value: "Time.newInstance(11, 34, 56, 0)",
            },
        );
        default_types.insert(
            String::from("Blob"),
            DefaultType {
                name: "Blob",
                parse: "parser.getBlobValue()",
                write: "gen.writeBlob(value)",
                test_value: "Blob.valueOf('test')",
            },
        );
        let mut sorted: Vec<(&String, &DefaultType)> = default_types.iter().collect();
        sorted.sort_by_key(|a| a.0);
        code_gen::write_utils(&mut zip, &prefix, &sorted)?;

        // write object classes

        for (obj_name, fields) in objs_by_name {
            code_gen::write_object(&mut zip, &prefix, &obj_name, fields, &default_types)?;
        }
        zip.finish().map_err(|e| {
            util::reply(
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("error writing zipfile: {}", e).into_bytes(),
            )
        })?;
    }
    cur.seek(std::io::SeekFrom::Start(0)).map_err(|e| {
        util::reply(
            http::StatusCode::INTERNAL_SERVER_ERROR,
            format!("error writing zipfile: {}", e).into_bytes(),
        )
    })?;
    cur.read_to_end(&mut buf).map_err(|e| {
        util::reply(
            http::StatusCode::INTERNAL_SERVER_ERROR,
            format!("error writing zipfile: {}", e).into_bytes(),
        )
    })?;

    Ok(http::Response::builder()
        .status(http::StatusCode::OK)
        .header("Content-Type", "application/zip")
        .header(
            "Content-Disposition",
            "attachment; filename=\"csv2apex.zip\"",
        )
        .body(buf)
        .unwrap())
}

async fn generate(
    mut form_data: warp::filters::multipart::FormData,
) -> Result<Resp, warp::reject::Rejection> {
    //tokio::time::sleep(std::time::Duration::new(5, 0)).await;

    /*
    let parts: Vec<warp::filters::multipart::Part> = match form_data.try_collect().await {
        Ok(parts) => parts,
        Err(e) => {
            return Ok(util::reply(
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("form error: {}", e).into_bytes(),
            ));
        }
    };
    */
    let mut prefix: Option<String> = None;
    let mut csv: Option<String> = None;
    while let Ok(Some(p)) = form_data.try_next().await {
        let name = String::from(p.name());
        if name != "prefix" && name != "csv" {
            // nevermind reading the contents, just quit
            return Ok(util::reply(
                http::StatusCode::BAD_REQUEST,
                format!("invalid parameter: {}", name).into_bytes(),
            ));
        }
        let value = match p
            .stream()
            .try_fold(Vec::new(), |mut vec, data| {
                debug!("read {} bytes for {}", data.chunk().len(), name);
                vec.extend_from_slice(data.chunk());
                async move { Ok(vec) }
            })
            .await
        {
            Ok(val) => val,
            Err(e) => {
                return Ok(util::reply(
                    http::StatusCode::INTERNAL_SERVER_ERROR,
                    format!("form error: {}", e).into_bytes(),
                ));
            }
        };
        if name == "prefix" {
            prefix = match String::from_utf8(value) {
                Ok(v) => {
                    if v.is_empty() {
                        None
                    } else {
                        Some(v)
                    }
                }
                Err(e) => {
                    info!("form error: {}", e);
                    return Ok(util::reply(
                        http::StatusCode::BAD_REQUEST,
                        format!("parameter {} is not valid utf-8", name).into_bytes(),
                    ));
                }
            };
        } else if name == "csv" {
            csv = match String::from_utf8(value) {
                Ok(v) => {
                    if v.is_empty() {
                        None
                    } else {
                        Some(v)
                    }
                }
                Err(e) => {
                    info!("form error: {}", e);
                    return Ok(util::reply(
                        http::StatusCode::BAD_REQUEST,
                        format!("parameter {} is not valid utf-8", name).into_bytes(),
                    ));
                }
            };
        }
    }
    let prefix = match prefix {
        Some(prefix) => prefix,
        None => {
            return Ok(util::reply(
                http::StatusCode::BAD_REQUEST,
                String::from("prefix is mandatory!").into_bytes(),
            ));
        }
    };
    let csv = match csv {
        Some(csv) => csv,
        None => {
            return Ok(util::reply(
                http::StatusCode::BAD_REQUEST,
                String::from("csv is mandatory!").into_bytes(),
            ));
        }
    };

    match process_csv(prefix, csv) {
        Ok(r) => Ok(r),
        Err(r) => Ok(r),
    }
}

async fn read_index() -> Result<Resp, Resp> {
    let mut file = tokio::fs::File::open("./data/index.html")
        .await
        .map_err(|e| {
            util::reply(
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("error reading index.html: {}", e).into_bytes(),
            )
        })?;
    let mut contents = vec![];
    tokio::io::AsyncReadExt::read_to_end(&mut file, &mut contents)
        .await
        .map_err(|e| {
            util::reply(
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("error reading index.html: {}", e).into_bytes(),
            )
        })?;
    if let Some(base_url) = std::env::var_os("BASEURL") {
        let mut body = String::from_utf8(contents).map_err(|e| {
            util::reply(
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("error reading index.html: {}", e).into_bytes(),
            )
        })?;
        let base_url = base_url.into_string().map_err(|e| {
            util::reply(
                http::StatusCode::INTERNAL_SERVER_ERROR,
                format!("Incorrectly configured base url: {:?}", e).into_bytes(),
            )
        })?;
        body = body.replace(
            "</title>",
            &format!("</title>\n\t\t<base href=\"{}\">", base_url),
        );
        contents = body.into_bytes();
    }
    Ok(http::Response::builder()
        .status(http::StatusCode::OK)
        .header("Content-Type", "text/html")
        .body(contents)
        .unwrap())
}

async fn serve_index() -> Result<Resp, warp::reject::Rejection> {
    match read_index().await {
        Ok(r) => Ok(r),
        Err(r) => Ok(r),
    }
}

#[tokio::main]
async fn main() {
    if std::env::var_os("RUST_LOG").is_none() {
        // Set `RUST_LOG=csv2apex=debug` to see debug logs,
        // this only shows access logs.
        std::env::set_var("RUST_LOG", "csv2apex=info");
    }
    env_logger::init();
    info!("Starting...");
    let port = match std::env::var("PORT") {
        Ok(val) => val.parse::<u16>().unwrap_or(8080),
        Err(_) => 8080,
    };
    let index = warp::get().and(warp::path::end()).and_then(serve_index);
    let static_dir = warp::path("static").and(warp::fs::dir("data/static"));
    let generate = warp::post().and(warp::multipart::form()).and_then(generate);
    let notfound = warp::any()
        .and(warp::fs::file("./data/notfound.html"))
        .map(|reply| warp::reply::with_status(reply, warp::http::StatusCode::NOT_FOUND));
    let log = warp::log("csv2apex");
    let routes = index.or(static_dir).or(generate).or(notfound).with(log);

    info!("listening on port {}", port);
    warp::serve(routes).run(([0, 0, 0, 0], port)).await;
}
