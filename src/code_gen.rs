/*
CSV2Apex - generate strongly typed apex classes for parsing from and serializing to JSON
Copyright (C) 2021  App Factory Group <support@appfactorygroup.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::cell::RefCell;
use std::collections::HashMap;
use std::io::Seek;
use std::io::Write;
use std::rc::Rc;

use crate::{DefaultType, Record, Resp};

use crate::util::{zip_start_file, zip_write};

pub fn write_base_object<W: Write + Seek>(
    zip: &mut zip::ZipWriter<W>,
    prefix: &str,
) -> Result<(), Resp> {
    zip_start_file(zip, format!("{}Object.cls", prefix))?;
    zip_write(
		zip,
		&format!(
			"public virtual class {0}Object {{
	private Set<String> touchedFields {{
		get {{
			if (touchedFields == null) {{
				touchedFields = new Set<String>();
			}}
			return touchedFields;
		}}
		set;
	}}

	public void touchField(String fieldName) {{
		this.touchedFields.add(fieldName);
	}}

	public void resetTouchedFields() {{
		this.touchedFields.clear();
	}}

	public void unTouchField(String fieldName) {{
		this.touchedFields.remove(fieldName);
	}}

	public Boolean isTouched(String fieldName) {{
		return this.touchedFields.contains(fieldName);
	}}

	private static final String UNKNWNPROPMSG = '{{0}} consuming unrecognized property \"{{1}}\" with value: {{2}}';

	public virtual void handleUnknownProperty(JSONParser parser, String objName, String fieldName) {{
		String value = {0}Utils.consumeObject(parser);
		System.debug(LoggingLevel.WARN, String.format(UNKNWNPROPMSG, new List<String>{{ objName, fieldName, value }}));
	}}

	public virtual void parse(JSONParser parser) {{
		System.JSONToken curr = parser.nextToken();
		while (curr != null && curr != System.JSONToken.END_OBJECT) {{
			if (curr == System.JSONToken.FIELD_NAME) {{
				String fieldName = parser.getText();
				curr = parser.nextToken();
				try {{
					parseField(parser, fieldName);
				}} catch (Exception exc)
				{{
					exc.setMessage('parseField failed on field \"' + fieldName + '\" with message: ' + exc.getMessage());
					throw exc;
				}}
			}}
			curr = parser.nextToken();
		}}
	}}

	public virtual void parseField(JSONParser parser, String fieldName) {{
		throw new {0}Exception('No parseField set for this object!');
	}}

	public virtual void parse(String jsonString) {{
		this.parse(JSON.createParser(jsonString));
	}}

	public virtual void toJsonString(JSONGenerator gen) {{
		throw new {0}Exception('No toString set for this object!');
	}}

	public virtual String toJsonString() {{
		JSONGenerator gen = JSON.createGenerator(false);
		try {{
			this.toJsonString(gen);
		}} catch (System.JSONException ex) {{
			System.debug(LoggingLevel.ERROR, ex.getMessage());
			System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
			System.debug(gen.getAsString());
			throw ex;
		}}
		return gen.getAsString();
	}}

	public virtual String getObjEndpoint() {{
		throw new {0}Exception('No endpoint specified');
	}}

	public virtual Type getType() {{return {0}Object.class;}}
}}
",
			prefix
		),
	)?;

    zip_start_file(zip, format!("{}ObjectTest.cls", prefix))?;
    zip_write(
        zip,
        &format!(
            "@isTest
public with sharing class {0}ObjectTest {{
	@isTest
	public static void testObject()
	{{
		Test.startTest();
		Boolean gotException = false;
		{0}Object obj = new {0}Object();
		obj.resetTouchedFields();
		obj.unTouchField('test');
		obj.handleUnknownProperty(JSON.createParser('{{}}'), 'test', 'test');
		try {{ obj.parse('{{\"test\":true}}'); }} catch ({0}Exception ex) {{gotException = true;}}
		System.assert(gotException);
		gotException = false;
		try {{ obj.toJsonString(); }} catch ({0}Exception ex) {{gotException = true;}}
		System.assert(gotException);
		gotException = false;
		Test.stopTest();
	}}
}}
",
            prefix
        ),
    )?;
    Ok(())
}

pub fn write_exception<W: Write + Seek>(
    zip: &mut zip::ZipWriter<W>,
    prefix: &str,
) -> Result<(), Resp> {
    zip_start_file(zip, format!("{}Exception.cls", prefix))?;
    zip_write(
        zip,
        &format!(
            "public with sharing class {0}Exception extends Exception {{}}\n",
            prefix
        ),
    )?;
    Ok(())
}

pub fn write_utils<W: Write + Seek>(
    zip: &mut zip::ZipWriter<W>,
    prefix: &str,
    default_types: &Vec<(&String, &DefaultType)>,
) -> Result<(), Resp> {
    zip_start_file(zip, format!("{}Utils.cls", prefix))?;
    zip_write(
        zip,
        &format!("public with sharing class {0}Utils {{", prefix),
    )?;
    zip_write(
        zip,
        "
	public static String consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		JSONGenerator gen = JSON.createGenerator(false);
		if (parser.getCurrentToken() == null) {
			// possibly nothing was consumed yet
			parser.nextToken();
		}
		do {
			System.JSONToken curr = parser.getCurrentToken();
			System.debug('Got Token ' + curr);
			switch on curr {
				when END_ARRAY {
					--depth;
					gen.writeEndArray();
				}
				when END_OBJECT {
					--depth;
					gen.writeEndObject();
				}
				when FIELD_NAME {
					gen.writeFieldName(parser.getText());
				}
				when START_ARRAY {
					++depth;
					gen.writeStartArray();
				}
				when START_OBJECT {
					++depth;
					gen.writeStartObject();
				}
				when VALUE_FALSE {
					gen.writeBoolean(false);
				}
				when VALUE_NULL {
					gen.writeNull();
				}
				when VALUE_NUMBER_FLOAT {
					gen.writeNumber(parser.getDecimalValue());
				}
				when VALUE_NUMBER_INT {
					gen.writeNumber(parser.getIntegerValue());
				}
				when VALUE_STRING {
					gen.writeString(parser.getText());
				}
				when VALUE_TRUE {
					gen.writeBoolean(true);
				}
				when else {
					if (curr != null) {
						throw new JsonException('Invalid token');
					}
				}
			}
		} while (depth > 0 && parser.nextToken() != null);
		return gen.getAsString();
	}",
    )?;

    zip_write(
        zip,
        &format!(
            "
	public static void writeObject(System.JSONGenerator gen, String name, {0}Object value) {{
		if (String.isNotBlank(name)) {{
			gen.writeFieldName(name);
		}}
		if (value == null) {{
			gen.writeNull();
		}} else {{
			value.toJsonString(gen);
		}}
	}}

	public static void writeObjectList(System.JSONGenerator gen, String name, List<{0}Object> values) {{
		if (String.isNotBlank(name)) {{
			gen.writeFieldName(name);
		}}
		if (values == null) {{
			gen.writeNull();
		}} else {{
			gen.writeStartArray();
			for ({0}Object value : values) {{
				{0}Utils.writeObject(gen, null, value);
			}}
			gen.writeEndArray();
		}}
	}}
",
            prefix
        ),
    )?;

    for (_key, typ) in default_types.iter() {
        zip_write(
            zip,
            &format!(
                "
	public static {0} parse{0}(System.JSONParser parser) {{
		if (parser.getCurrentToken() == System.JSONToken.VALUE_NULL) {{
			return null;
		}}
		return {1};
	}}
	public static void write{0}(System.JSONGenerator gen, String name, {0} value) {{
		if (String.isNotBlank(name)) {{
			gen.writeFieldName(name);
		}}
		if (value == null) {{
			gen.writeNull();
		}} else {{
			{2};
		}}
	}}
	public static List<{0}> parseList{0}(System.JSONParser parser) {{
		System.JSONToken curr = parser.getCurrentToken();
		if (parser.getCurrentToken() == null) {{
			// possibly nothing was consumed yet
			curr = parser.nextToken();
		}}
		if (curr == System.JSONToken.VALUE_NULL) {{
			return null;
		}}
		if (curr != System.JSONToken.START_ARRAY) {{
			throw new System.JSONException('Error while parsing List, Expected START_ARRAY, got ' + curr);
		}}
		List<{0}> rv = new List<{0}>();
		curr = parser.nextToken();
		while (curr != null && curr != System.JSONToken.END_ARRAY) {{
			rv.add({3}Utils.parse{0}(parser));
			curr = parser.nextToken();
		}}
		return rv;
	}}
	public static void writeList{0}(System.JSONGenerator gen, String name, List<{0}> values) {{
		if (String.isNotBlank(name)) {{
			gen.writeFieldName(name);
		}}
		gen.writeStartArray();
		for ({0} value : values) {{
			{3}Utils.write{0}(gen, null, value);
		}}
		gen.writeEndArray();
	}}
",
                typ.name, typ.parse, typ.write, prefix
            ),
        )?
    }
    zip_write(zip, "}\n")?;

    zip_start_file(zip, format!("{}UtilsTest.cls", prefix))?;
    zip_write(
		zip,
		&format!(
"@isTest
public with sharing class {0}UtilsTest {{
	@isTest
	public static void testUtils()
	{{
		Test.startTest();
		String input = '{{\"foo\":\"bar\",\"1\":1,\"1.2\":1.2,\"true\":true,\"false\":false,\"null\":null,\"obj\":{{\"test\":\"Obj\"}},\"arr\":[1,2,3]}}';
		String output = {0}Utils.consumeObject(JSON.createParser(input));
		System.debug(output);
		System.assertEquals(input, output);
		System.JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		{0}Utils.writeListString(gen, 'String', {0}Utils.parseListString(JSON.createParser('[\"string\", null]')));
		{0}Utils.writeListInteger(gen, 'Integer', {0}Utils.parseListInteger(JSON.createParser('[1, null]')));
		{0}Utils.writeListLong(gen, 'Long', {0}Utils.parseListLong(JSON.createParser('[9876543210, null]')));
		{0}Utils.writeListDouble(gen, 'Double', {0}Utils.parseListDouble(JSON.createParser('[1.2, null]')));
		{0}Utils.writeListDecimal(gen, 'Decimal', {0}Utils.parseListDecimal(JSON.createParser('[3.14, null]')));
		{0}Utils.writeListBoolean(gen, 'Boolean', {0}Utils.parseListBoolean(JSON.createParser('[true, null]')));
		{0}Utils.writeListDate(gen, 'Date', {0}Utils.parseListDate(JSON.createParser('[\"2021-05-23\", null]')));
		{0}Utils.writeListDatetime(gen, 'Datetime', {0}Utils.parseListDatetime(JSON.createParser('[\"2021-05-23T11:34:45.678Z\", null]')));
		{0}Utils.writeListTime(gen, 'Time', {0}Utils.parseListTime(JSON.createParser('[\"11:34:56\", null]')));
		{0}Utils.writeListId(gen, 'Id', {0}Utils.parseListId(JSON.createParser('[\"001S000001Np8YKIAZ\", null]')));
		{0}Utils.writeListBlob(gen, 'Blob', {0}Utils.parseListBlob(JSON.createParser('[\"YmxvYg==\", null]')));
		gen.writeEndObject();
		System.debug(gen.getAsString());
		gen = JSON.createGenerator(true);
		gen.writeStartObject();
		try {{
			{0}Utils.writeObjectList(gen, 'object', new List<{0}Object>{{null, new {0}Object()}});
		}} catch ({0}Exception exc) {{
		}}
		Test.stopTest();
	}}
}}
", prefix))?;

    Ok(())
}

pub fn write_object<W: Write + Seek>(
    zip: &mut zip::ZipWriter<W>,
    prefix: &str,
    obj_name: &str,
    fields: Rc<RefCell<Vec<Record>>>,
    default_types: &HashMap<String, DefaultType>,
) -> Result<(), Resp> {
    let mut endpoint: Option<&str> = None;
    zip_start_file(zip, format!("{}{}.cls", prefix, obj_name))?;
    zip_write(
        zip,
        &format!(
            "public with sharing class {0}{1} extends {0}Object {{\n",
            prefix, obj_name
        ),
    )?;
    let fields = (*fields).borrow();
    zip_write(zip, &format!("	public {}{}() {{}}\n", prefix, obj_name))?;
    zip_write(
        zip,
        &format!(
            "	public {}{}(String json) {{\n		this.parse(json);\n	}}\n",
            prefix, obj_name
        ),
    )?;
    zip_write(
        zip,
        &format!(
            "	public {}{}(JSONParser parser) {{\n		this.parse(parser);\n	}}\n\n",
            prefix, obj_name
        ),
    )?;
    for record in fields.iter() {
        if let Some(val) = &record.endpoint {
            endpoint = Some(&val);
        }
        let apex_name = record.apex_name.as_ref().unwrap();
        let description = record.description.clone().unwrap_or(String::new());
        let description = if !description.is_empty() {
            format!("	/* {0} */\n", description.replace("\n", "\n	* "))
        } else {
            String::new()
        };
        if record.list.as_ref().unwrap() == "true" {
            zip_write(zip, &format!("{2}	@AuraEnabled\n	public List<{0}> {3} {{get; set {{ this.touchField('{1}'); this.{3} = value;}}}}\n", record.data_type, record.field, description, apex_name))?;
        } else {
            zip_write(zip, &format!("{2}	@AuraEnabled\n	public {0} {3} {{get; set {{ this.touchField('{1}'); this.{3} = value;}}}}\n", record.data_type, record.field, description, apex_name))?;
        }
    }
    // json parser

    zip_write(
        zip,
        "\n	public override void parseField(JSONParser parser, String fieldName) {\n",
    )?;

    for (idx, record) in fields.iter().enumerate() {
        let apex_name = record.apex_name.as_ref().unwrap();
        zip_write(zip, "		")?;
        if idx > 0 {
            zip_write(zip, "} else ")?;
        }
        zip_write(zip, &format!("if (fieldName == '{0}') {{\n", record.field))?;
        if default_types.contains_key(&record.data_type) {
            if record.list.as_ref().unwrap() == "true" {
                zip_write(
                    zip,
                    &format!(
                        "			this.{0} = {1}Utils.parseList{2}(parser);\n",
                        apex_name, prefix, record.data_type
                    ),
                )?;
            } else {
                zip_write(
                    zip,
                    &format!(
                        "			this.{0} = {1}Utils.parse{2}(parser);\n",
                        apex_name, prefix, record.data_type
                    ),
                )?;
            }
        } else {
            if record.list.as_ref().unwrap() == "true" {
                zip_write(
                    zip,
                    &format!(
                        "			this.{0} = {1}.parseList(parser);\n",
                        apex_name, record.data_type
                    ),
                )?;
            } else {
                zip_write(
                    zip,
                    &format!("			this.{0} = (parser.getCurrentToken() == System.JSONToken.VALUE_NULL ? null : new {1}(parser));\n", apex_name, record.data_type,),
                )?;
            }
        }
    }
    zip_write(
        zip,
        &format!(
            "		}} else {{
			handleUnknownProperty(parser, '{0}{1}', fieldName);
		}}
	}}

",
            prefix, obj_name
        ),
    )?;

    // json generator

    zip_write(
        zip,
        "	public override void toJsonString(JSONGenerator gen) {
		gen.writeStartObject();\n",
    )?;
    for record in fields.iter() {
        let apex_name = record.apex_name.as_ref().unwrap();
        zip_write(
            zip,
            &format!("		if (this.isTouched('{0}')) {{\n", record.field),
        )?;
        if default_types.contains_key(&record.data_type) {
            if record.list.as_ref().unwrap() == "true" {
                zip_write(
                    zip,
                    &format!(
                        "			{1}Utils.writeList{2}(gen, '{0}', this.{3});
		}}\n",
                        record.field, prefix, record.data_type, apex_name
                    ),
                )?;
            } else {
                zip_write(
                    zip,
                    &format!(
                        "			{1}Utils.write{2}(gen, '{0}', this.{3});
		}}\n",
                        record.field, prefix, record.data_type, apex_name
                    ),
                )?;
            }
        } else {
            if record.list.as_ref().unwrap() == "true" {
                zip_write(
                    zip,
                    &format!(
                        "			{1}Utils.writeObjectList(gen, '{0}', this.{2});
		}}\n",
                        record.field, prefix, apex_name
                    ),
                )?;
            } else {
                zip_write(
                    zip,
                    &format!(
                        "			{1}Utils.writeObject(gen, '{0}', this.{2});
		}}\n",
                        record.field, prefix, apex_name
                    ),
                )?;
            }
        }
    }
    zip_write(
        zip,
        "		gen.writeEndObject();
	}\n",
    )?;

    zip_write(
        zip,
        &format!(
            "
	public static List<{0}{1}> parseList(System.JSONParser parser) {{
		System.JSONToken curr = parser.getCurrentToken();
		if (parser.getCurrentToken() == null) {{ // possibly nothing was consumed yet
			curr = parser.nextToken();
		}}
		if (curr == System.JSONToken.VALUE_NULL) {{
			return null;
		}}
		if (curr != System.JSONToken.START_ARRAY) {{
			throw new System.JSONException('Error while parsing List, Expected START_ARRAY, got ' + curr);
		}}
		List<{0}{1}> rv = new List<{0}{1}>();
		curr = parser.nextToken();
		while (curr != null && curr != System.JSONToken.END_ARRAY) {{
			rv.add(curr == System.JSONToken.VALUE_NULL ? null : new {0}{1}(parser));
			curr = parser.nextToken();
		}}
		return rv;
	}}

	public static List<{0}{1}> convertList(List<{0}Object> objs) {{
		List<{0}{1}> rv = new List<{0}{1}>();
		for ({0}Object obj: objs) {{
			rv.add(({0}{1})obj);
		}}
		return rv;
	}}

	public override Type getType() {{return {0}{1}.class;}}

",
            prefix, obj_name
        ),
    )?;

    if let Some(endpoint) = endpoint {
        zip_write(
            zip,
            &format!(
                "	public override String getObjEndpoint() {{return '{0}';}}\n\n",
                endpoint
            ),
        )?;
    }

    zip_write(zip, "}")?;

    // TestClass

    zip_start_file(zip, format!("{}{}Test.cls", prefix, obj_name))?;
    zip_write(
        zip,
        &format!(
            "@isTest
public with sharing class {0}{1}Test {{
	@isTest
	public static void test{1}()
	{{
		Test.startTest();
		{0}{1} obj = new {0}{1}();
		System.assertEquals({0}{1}.class, obj.getType());
",
            prefix, obj_name
        ),
    )?;
    for record in fields.iter() {
        let apex_name = record.apex_name.as_ref().unwrap();
        if let Some(def_typ) = default_types.get(&record.data_type) {
            if record.list.as_ref().unwrap() == "true" {
                zip_write(
                    zip,
                    &format!(
                        "		obj.{0} = new List<{1}>{{{2}}};\n",
                        apex_name, record.data_type, def_typ.test_value
                    ),
                )?;
            } else {
                zip_write(
                    zip,
                    &format!("		obj.{0} = {1};\n", apex_name, def_typ.test_value),
                )?;
            }
        } else {
            if record.list.as_ref().unwrap() == "true" {
                zip_write(
                    zip,
                    &format!(
                        "		obj.{0} = new List<{1}>{{new {1}()}};\n",
                        apex_name, record.data_type
                    ),
                )?;
            } else {
                zip_write(
                    zip,
                    &format!("		obj.{0} = new {1}();\n", apex_name, record.data_type),
                )?;
            }
        }
    }

    if endpoint.is_some() {
        zip_write(zip, "		obj.getObjEndpoint();\n")?;
    }
    zip_write(
        zip,
        &format!(
            "
		System.JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartArray();
		obj.toJsonString(gen);
		gen.writeEndArray();
		{0}{1}.parseList(JSON.createParser(gen.getAsString()));
		Boolean hasError = false;
		try {{
			{0}{1}.parseList(JSON.createParser('{{}}'));
		}} catch (System.JSONException jsonExc) {{
			hasError = true;
		}}
		System.assert(hasError, 'no start array error');
		hasError = false;
		try {{
			{0}{1}.parseList(JSON.createParser('[{{}},]'));
		}} catch (System.JSONException jsonExc) {{
			hasError = true;
		}}
		System.assert(hasError, 'no end array error');
		{0}{1}.convertList(new List<{0}Object>{{new {0}{1}('{{\"___NONSENSE___\":true}}')}});
		Test.stopTest();
	}}
}}",
            prefix, obj_name
        ),
    )?;

    Ok(())
}
